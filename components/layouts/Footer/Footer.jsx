import React from "react";
import Link from "next/link";

import styles from "./Footer.module.scss";

const Footer = () => {
  return (
    <footer className={styles.container}>
      <div className={styles.col}>
        <div className={styles.wrap}>
          <strong>Sign up to receive new releases</strong>
        </div>
        <p className={styles.copyright}>© 2023 Todd Shelton</p>
      </div>
      <div className={styles.col}>
        <div className={styles.colChild}>
          <strong>Company</strong>
          <ul>
            <li>
              <a href="https://toddshelton.com/st/about">About</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/reviews">Reviews</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/help">Help</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/privacy-policy">
                Privacy Policy
              </a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/affiliate">Affiliates</a>
            </li>
            <li>
              <a href="https://dashboard.accessibe.com/statement?domain=toddshelton.com">
                Accessibility
              </a>
            </li>
          </ul>
          <strong>Programs</strong>
          <ul>
            <li>
              <a href="https://toddshelton.com/st/home-try-on">Home Try-On</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/fit-recommendation">
                Fit recomendation
              </a>
            </li>
          </ul>
        </div>
        <div className={styles.colChild}>
          <strong>Products</strong>
          <ul>
            <li>
              <a href="https://toddshelton.com/st/jeans">Jeans</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/shirts">Shirts</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/pants">Pants</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/t-shirts">T-Shirts</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/shorts">Shorts</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/sweatshirts">Sweatshirts</a>
            </li>
            <li className={styles.devider}>
              <a href="https://toddshelton.com/st/repair">Repairs</a>
            </li>
            <li>
              <a href="https://toddshelton.com/st/gifts/gift">Gift card</a>
            </li>
            <li className={styles.devider}>
              <a href="https://toddshelton.com/st/all-shops">Discontinued</a>
            </li>
          </ul>
        </div>
        <div className={styles.colChild}>
          <strong>Contact</strong>
          <div className={styles.wrap}>
            <a href="tel:+1 844-626-6355">(844) 626-6355 (M-F, 9-3 ET)</a>
            <br />
            <a href="mailto:support@toddshelton.com">support@toddshelton.com</a>
          </div>
          <div className={styles.wrap}>
            <p>
              450 Murray Hill Parkway, C2
              <br />
              East Rutherford, NJ 07073
            </p>
          </div>
        </div>
      </div>
      <p className={`${styles.copyright} showOnTablet`}>© 2023 Todd Shelton</p>
    </footer>
  );
};

export default Footer;
