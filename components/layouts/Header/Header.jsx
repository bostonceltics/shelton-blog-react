import React, { useState, useEffect } from "react";
import { useStateContext } from "../../../context/StateContext";
import Link from "next/link";

import styles from "./Header.module.scss";

const Header = () => {
  const { showBlock, toggleBlock, isSticky } = useStateContext();

  const stickyClass = isSticky ? styles.sticky : "";

  return (
    <>
      <header
        className={styles.header}
        style={{ paddingBottom: stickyClass ? "50px" : "" }}
      >
        <div className={`${styles.wrap} ${styles.mobSticky}`}>
          <div className={`${styles.container} ${styles.heightTop}`}>
            <Link href="/" className={styles.logo}>
              Todd Shelton
            </Link>

            <ul>
              <li>
                <Link href="#">About</Link>
              </li>
            </ul>

            {!showBlock ? (
              <div className={styles.burger} onClick={toggleBlock}></div>
            ) : (
              <div className={styles.close} onClick={toggleBlock}></div>
            )}
          </div>
        </div>
        <div className={stickyClass}>
          <div className={styles.wrap}>
            <div className={`${styles.container} ${styles.heightBottom}`}>
              <ul>
                <li>
                  <Link href="">Jeans</Link>
                </li>
                <li>
                  <Link href="">Shirts</Link>
                </li>
                <li>
                  <Link href="">Pants</Link>
                </li>
                <li>
                  <Link href="">T-Shirts</Link>
                </li>
                <li
                  className={`${styles.more} ${
                    showBlock && styles.more__active
                  }`}
                  onClick={toggleBlock}
                >
                  <b></b>
                  <b></b>
                  <b></b>
                </li>
              </ul>
            </div>
          </div>
          {showBlock && (
            <div className={styles.wrap}>
              <div className={`${styles.container} ${styles.hiddenMenu}`}>
                <ul>
                  <li className="showOnMobile">
                    <Link href="">Jeans</Link>
                  </li>
                  <li className="showOnMobile">
                    <Link href="">Shirts</Link>
                  </li>
                  <li className="showOnMobile">
                    <Link href="">Pants</Link>
                  </li>
                  <li className="showOnMobile">
                    <Link href="">T-Shirts</Link>
                  </li>
                  <li>
                    <Link href="">Shorts</Link>
                  </li>
                  <li>
                    <Link href="">Sweatshirts</Link>
                  </li>
                </ul>
                <ul className={styles.menuDevider}>
                  <li>
                    <Link href="">Repairs</Link>
                  </li>
                  <li>
                    <Link href="">Gift card</Link>
                  </li>
                </ul>
                <ul className={styles.menuDevider}>
                  <li>
                    <Link href="">Discontinued</Link>
                  </li>
                </ul>
                <ul className={`${styles.menuDevider} showOnMobile`}>
                  <li>
                    <Link href="">Fit recomendation</Link>
                  </li>
                  <li>
                    <Link href="">Home Try-On</Link>
                  </li>
                </ul>
                <ul className={`${styles.menuDevider} showOnMobile`}>
                  <li>
                    <Link href="">About</Link>
                  </li>
                  <li>
                    <Link href="">Help</Link>
                  </li>
                </ul>
              </div>
            </div>
          )}
        </div>
      </header>
    </>
  );
};

export default Header;
