import React, { createContext, useContext, useState, useEffect } from "react";
import axios from "axios";

const Context = createContext();

export const StateContext = ({ children }) => {
  // start toggleBlock function

  const [showBlock, setShowBlock] = useState(false);

  const toggleBlock = () => {
    setShowBlock(!showBlock);
  };

  // end toggleBlock function

  // start sticky header

  const [isSticky, setSticky] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.innerWidth >= 768 && window.scrollY > 55) {
        setSticky(true);
      } else {
        setSticky(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  // end sticky header

  return (
    <Context.Provider
      value={{
        showBlock,
        toggleBlock,
        isSticky,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useStateContext = () => useContext(Context);
